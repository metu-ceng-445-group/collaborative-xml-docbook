from pprint import pprint
from socket import *
from threading import Thread, Condition
import time

message_received = Condition()
is_id_valid = False

def notify_service(sock):
    global message_received
    global is_id_valid
    req = sock.recv(10000)

    while req and req != '':
        # remove trailing newline and blanks
        req = req.rstrip()
        pprint(req.decode())
        if req.decode() == 'The id you entered is invalid.':
            is_id_valid = False
        if req.decode() == 'DBDoc initialized, notify_service created.':
            is_id_valid = True
        with message_received:
            time.sleep(0.3)
            message_received.notify()

        req = sock.recv(10000)

    print(sock.getpeername(), ' closing')

def client(port):
    global is_id_valid
    # the connection is kept alive until client closes it.
    connection = socket(AF_INET, SOCK_STREAM)
    connection.connect(('127.0.0.1', port))

    ns = Thread(target=notify_service, args=(connection,))
    ns.start()
    with message_received:
        message_received.wait()
    DBDoc_id = input("Please give the id of document you would like to connect "
                     "\n(You can write 'NEW' to create a new DBDoc.):\n")

    connection.send(DBDoc_id.encode())
    with message_received:
        message_received.wait()
    while not is_id_valid:
        DBDoc_id = input("Please give the id of document you would like to connect "
                         "\n(You can write 'NEW' to create a new DBDoc.):\n")

        connection.send(DBDoc_id.encode())
        with message_received:
            message_received.wait()

    while True:
        command = input('\nPlease write your command. (You can write "exit" to end connection):\n')

        if command == 'exit':
            print('exitting')
            connection.send('exit'.encode())
            break
        connection.send(command.encode())
        with message_received:
            message_received.wait()
def run():
    client(20446)

