from globals.SharedVariables import SharedVariables
from dbd.DBDController import DBDController
from dbd.DBDPersistency import DBDPersistency
from pprint import pprint

from config import DBConnector

xml_doc_example = '''<page xmlns="http://projectmallard.org/1.0/"
      type="guide"
      id="index">
<title>This is the uploaded xml</title>
<info>
<link type="guide" id="3" xref="index#new-project"/>
<title type="sort">3</title>
<desc>Write a video to a DVD or SVCD.</desc>
<credit type="author">
<name id="nameId">Ekaterina Gerasimova</name>
<email>kittykat3756@googlemail.com</email>
</credit>
<license>
<p>Creative Commons Share Alike 3.0</p>
</license>
</info>
5
METU Department of Computer Engineering
<title>Create a video project</title>
<p><app>Brasero</app> can be used to create video discs for playing in a DVD
player or laptop.</p>
<steps>
<item>
<p>Click <gui>Video project</gui> on the start page, or select
<guiseq><gui>Project</gui><gui>New Project</gui><gui>New Video
Project</gui></guiseq>.</p>
</item>
<item>
<p>Add the videos to the project by clicking <gui>Add</gui> in the
toolbar and selecting the files. You can also add files by
dragging and dropping them onto the project area or by clicking
<guiseq><gui>Edit</gui><gui>Add Files</gui></guiseq>.</p>
</item>
<item>
<p>You can add a title for the disc in the text entry field below the
project area.</p>
</item>
<item>
<p>Select the blank disc in the drop down list.</p>
</item>
<item>
<p>Click <gui>Burn...</gui> to continue.</p>
</item>
<item>
<p>Select the <gui>Burning speed</gui> from the drop down list, and any
other options you may want.</p>
</item>
<item>
<p>Click <gui>Burn</gui> to burn a single disc of the project or
<gui>Burn Several Copies</gui> to burn your project to multiple discs.</p>
</item>
</steps>
</page>'''
info_message = '''
Press: \n
c -> continue 
0 -> print database content 
1 -> print changed-unchaged documents 
2 -> print only changed documents 
3 -> print all documents in detail (use this to see the content of the documents)
'''
def pretty_print(object, detailed=False):
    try:
        if detailed:
            print("------------------")
            print("----Object is:----")
        pprint(vars(object))
        print("-----Content:-----")
        print(object)
        print("------------------")

    except:
        print("------------------")
        print("This can't be printed")
        print("------------------")

def take_input():
    while True:
        x = input('\n' + info_message + '\n')
        if x == 'c':
            return
        elif x == '0':
            pprint(DBDPersistency.list())
        elif x == '1':
            pprint(SharedVariables.get_all_dbdoc())
        elif x == '2':
            pprint(DBDPersistency.listmem(True))
        elif x == '3':
            dict = {}
            for i in SharedVariables.get_all_dbdoc():
                dbdoc = SharedVariables.get_all_dbdoc()[i]['content']
                changed = SharedVariables.get_all_dbdoc()[i]['changed']
                json = {'Changed': changed, 'Name': dbdoc.get_name(), 'Content':str(dbdoc)}
                dict[i] = json
            pprint(dict)

def begin():

    print("Starting test of DBDController: ")
    DBConnector.get_connection().cursor().execute('DELETE FROM xml_doc')
    DBConnector.get_connection().commit()
    controller_db = DBDController()
    controller_db.set_name("This is the xml document from database")
    DBDPersistency.save(controller_db.get_id())

    print("A dummy element is saved to db. Here is the current database content: \n")
    pprint(DBDPersistency.list())

    take_input()

    print("\n\n\n\n\n----------- PHASE 1 --------\n")
    print("This phase creates DBDController objects and edits names of the documents to show\n "
          "the behaviour of the program when more than one person is connected to the same document.\n\n")
    print("Creating DBDController Objects...")

    controller_1 = DBDController()
    controller_2 = DBDController()
    controller_3 = DBDController(attached_id=controller_1.get_id())

    controllers = [controller_1, controller_2, controller_3]

    print("DBDController Objects Created. \nThere are {} objects present \n".format(len(controllers)))

    for controller in controllers:
        print("controller_{} attached to {}".format(controller.count, controller.get_id()))

    print("\n! Note that controller_3 is attached to the same document as controller_1 !\n")
    print("Controller 1 and Controller 2 changed their document's names \n")
    controller_1.set_name('Name_A')
    controller_2.set_name('Name_B')

    take_input()
    print('\n--' * 6)
    print('\nCheck whether change made by one is reflected to others.')

    controller_3.set_name('Name_C')  # attached to the same DBDoc as controller_1

    print('\nContent of Controller_1s document after Controller_3 made a change (setting name to Name_C)\n ')
    print(controller_1)

    if controller_1.get_name() != 'Name_C' or controller_3.get_name() != 'Name_C':
        print('Phase 1 failed. Controller{} and Controller{} do not have the same name.'.format(controller_1.count,
                                                                                                controller_3.count))
        exit(1)

    take_input()
    print('--' * 6)

    print("\n\n------------ PHASE 2 --------------------\n\n")
    print("This phase is for editing xml documents in memory.\n"
          " In this phase controller_1 and controller_3 are editing the same document in turns\n\n")
    print('Currently xml documents which are being edited are:\n ')
    pprint(DBDPersistency.listmem(True))

    # controller_1 and controller_3 are editing the same document

    print("--------------------------------------------------------------------")
    print("insert_child , insert_sibling functions are being used:")
    print("--------------------------------------------------------------------")

    title_id = controller_1.insert_child(controller_1.get_id(), "title")[0]
    section_id = controller_3.insert_child(controller_3.get_id(), "section")[0]
    info_id = controller_1.insert_child(controller_1.get_id(), "info", append=False)[0]
    link_id = controller_3.insert_child(info_id, "link")[0]

    credit_id = controller_1.insert_sibling(link_id, "credit")[0]
    revision_id = controller_3.insert_sibling(credit_id, "revision", after=False)[0]
    desc_id = controller_1.insert_sibling(credit_id, "desc")[0]

    name_id = controller_3.insert_child(credit_id, "name")[0]
    email_id = controller_1.insert_child(credit_id, "email")[0]
    media_id = controller_3.insert_child(section_id, "media")[0]
    p1_id = controller_1.insert_child(media_id, "p")[0]
    p2_id = controller_1.insert_child(media_id, "p")[0]

    print("Resulting xml document of id '{}' after insert sibling and child operations".format(controller_1.get_id()))
    print(controller_1.dbdoc)

    take_input()

    print("--------------------------------------------------------------------")
    print("set_element_attribute function is being used:")
    print("--------------------------------------------------------------------")

    controller_3.set_element_attr(link_id, "type", "guide")
    controller_1.set_element_attr(link_id, "xref", "index")
    controller_3.set_element_attr(revision_id, "version", "16.04")
    controller_1.set_element_attr(revision_id, "date", "2016-03-17")
    controller_3.set_element_attr(revision_id, "status", "review")
    controller_3.set_element_attr(credit_id, "type", "author copyright")
    controller_1.set_element_attr(media_id, "type", "image")
    controller_3.set_element_attr(media_id, "mime", "image/png")
    controller_1.set_element_attr(media_id, "src", "test.png")
    controller_3.set_element_attr(media_id, "width", "18")
    controller_1.set_element_attr(media_id, "height", "18")

    print("Resulting xml document of id '{}' after set_element_attribute".format(controller_1.get_id()))
    print(controller_1.dbdoc)

    take_input()

    print("--------------------------------------------------------------------")
    print("set_element_text and insert_text function is being used:")
    print("--------------------------------------------------------------------")

    controller_3.set_element_text(name_id, "Shaun McCance")
    controller_1.set_element_text(email_id, "shaunm@gnome.org")
    controller_3.set_element_text(desc_id, "Add input sources and switch between them.")
    controller_3.set_element_text(title_id, "Use alternative input sources")
    controller_3.set_element_text(p1_id, "example media 1")
    controller_1.set_element_text(p2_id, "example media 2")

    controller_1.set_element_text(section_id, "You should")
    em_id = controller_3.insert_child(section_id, "em", append=False)[0]
    controller_3.set_element_text(em_id, "never")
    controller_1.insert_text(em_id, "run a graphical application as root.")

    print("Resulting xml document of id '{}' after set_element_text and insert_text".format(controller_1.get_id()))
    print(controller_1.dbdoc)

    take_input()

    print("--------------------------------------------------------------------")
    print("get_element_by_id, get_element_by_xpath functions are being used:")
    print("--------------------------------------------------------------------")

    print("Getting element with id = {} which has the tag 'credit'".format(credit_id))

    element = controller_3.get_element_by_id(credit_id)
    print(element)

    print("Getting element with path = 'info/credit/name[1]' which has the tag 'name' ")

    element = controller_3.get_element_by_xpath('info/credit/name[1]')
    print(element)

    print("Getting the second element with path = 'section/media/p[2]' which has the tag 'p' ")

    element = controller_3.get_element_by_xpath('section/media/p[2]')
    print(element)

    take_input()

    print("--------------------------------------------------------------------")
    print("delete_element_by_id, delete_element_attr functions are being used:")
    print("--------------------------------------------------------------------")

    print("Removing attributes of revision tag that has id = {}".format(revision_id))

    controller_1.delete_element_attr(revision_id, "version")
    controller_1.delete_element_attr(revision_id, "date")
    controller_1.delete_element_attr(revision_id, "status")
    controller_1.delete_element_attr(revision_id, "not_exists")  # should do nothing but returning an error string

    print("Resulting xml document of id '{}' after delete_element_attr".format(controller_1.get_id()))
    print(controller_1.dbdoc)

    take_input()

    print("Deleting the 'section' tag that has id {}".format(section_id))
    print("\n! Note that section has child nodes which should also be deleted with this command !\n")

    controller_3.delete_element_by_id(section_id)

    print("Resulting xml document of id '{}' after delete_element_by_id".format(controller_1.get_id()))
    print(controller_1.dbdoc)

    take_input()

    print("Uploading an xml document to controller_3.")

    controller_3.upload(xml_doc_example)

    take_input()

    pretty_print(controller_3)
    controller_temp = controller_3
    print("Note that, now controller_3 is attached to a different document than controller_1\n")

    take_input()

    print("Saving uploaded DBDoc to Database\nHere is the database content:\n")

    DBDPersistency.save(controller_3.get_id())
    pprint(DBDPersistency.list())

    take_input()

    print("\n\n------------ PHASE 3 --------------------\n\n")
    print("\nThis phase is for exceptional cases that should be caught by the program")


    print("delete_element_by_id()\n")

    print(controller_3.delete_element_by_id("Not_exists")[1]+"\n")

    print("delete_element_attr()\n")

    print(controller_3.delete_element_attr(controller_3.get_id(),"id")[1])
    print(controller_3.delete_element_attr(controller_3.get_id(),"NotExists")[1])
    print(controller_3.delete_element_attr("NotExists","attr")[1]+"\n")

    print("set_element_attr()\n")

    print(controller_3.set_element_attr(controller_3.get_id(),"id", "newId")[1])
    print(controller_3.set_element_attr(controller_3.get_id(),"id", "newId")[1])
    print(controller_3.set_element_attr("NotExists","ne", "ne")[1]+"\n")

    take_input()

    print("set_element_text()\n")

    print(controller_3.set_element_text("NotExists","ne")[1]+"\n")

    print("insert_child()\n")

    print(controller_3.insert_child(controller_3.get_id(), None)[1])
    print(controller_3.insert_child("NotExists", "ne")[1] + "\n")

    print("insert_sibling()\n")

    print(controller_3.insert_sibling(controller_3.get_id(),None)[1])
    print(controller_3.insert_sibling("NotExists", "ne")[1] + "\n")

    print("insert_text()\n")
    print(controller_3.insert_text("NotExists", "ne")[1] + "\n")

    take_input()

    print("\n\n------------ PHASE 4 --------------------\n\n")
    print("\nThis phase is for database operations save, load and delete.\n")

    print(('Load the xml with id {}\n from db and attach controller_3 to it.\n'.format(
        controller_db.get_id())))

    controller_3 = DBDController(controller_db.get_id())
    print(' \nNote that when an xml is loaded from db, its status is unchanged by default \n')
    pprint(SharedVariables.get_all_dbdoc())

    take_input()

    print('\nChange controller_1`s xml name to -New Name- and save to db.\n')
    print('\n--' * 6)
    controller_1.set_name('New name')
    DBDPersistency.save(controller_1.get_id())
    print('\nYou may want to see database content at this step\n')
    take_input()
    print('\nNote that when an xml is saved to database, its status is no more --changed--.\n')
    pprint(SharedVariables.get_all_dbdoc())

    take_input()

    print('\nChange controller_1`s xml name to -Some Random Name- and insert an element \n')
    controller_1.set_name('Some Random Name')
    controller_1.insert_child(controller_1.get_id(), 'tag')
    print('\nController 1 document after some changes made\n', controller_1)
    pprint(SharedVariables.get_all_dbdoc())

    take_input()

    print('\nLoad the same xml from db. Changes should be overwritten')
    DBDPersistency.load(controller_1.get_id())
    print(controller_1)
    pprint(SharedVariables.get_all_dbdoc())

    take_input()

    print("Database content:")
    pprint(DBDPersistency.list())
    print("Deleting database content")

    DBDPersistency.delete(controller_1.get_id())
    DBDPersistency.delete(controller_2.get_id())
    DBDPersistency.delete(controller_3.get_id())
    DBDPersistency.delete(controller_temp.get_id())


    print("Database is now empty \n")
    print("Database content:")
    pprint(DBDPersistency.list())



    # print("------------- PHASE 4 ---------------------- \n")
    #
    # print(
    #     " In this phase controller_4 and controller_5 is attached to the same xml.\n If any change occurs in one, it should be reflected to other.\n")
    # controller_4 = DBDController()
    # controller_5 = DBDController(controller_4.get_id())
    # print('INITIAL CONTENT: \n')
    # pretty_print(controller_4)
    # controller_5.insert_child(controller_4.get_id(), 'name')
    # tag_1_id = controller_4.insert_child('0', 'tag_1')
    # tag_2_id = controller_5.insert_child('0', 'tag_2')
    # tag_3_id = controller_4.insert_child('0', 'tag_3', append=False)
    # print('CONTENT AFTER PHASE_3 FINISHES \n')
    # pretty_print(controller_5)
    #
    # if str(controller_5) != str(controller_4):
    #     print("Phase 3 failed, controller_4's xml and controller_5's xml are not the same.")
    #     exit(1)
