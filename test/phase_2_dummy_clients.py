import random
import time
from pprint import pprint
from threading import Condition, Thread

from config import DBConnector
from dbd.DBDController import DBDController
from dbd.DBDPersistency import DBDPersistency

CLIENT_COUNT = 8
dbdoc = DBDController()
dbdoc_id = dbdoc.get_id()  # connect all clients to this id for concurrency
stopper = Condition()
arrived = Condition()
arrived_count = 0
is_finished = False


def client():
    global dbdoc_id
    global stopper
    global arrived
    global is_finished

    tag_list = ['info', 'title', 'section']
    attrib_list = ['id', 'type', 'xmlns']
    loop_count = 5
    added_children_list = []
    controller = DBDController(dbdoc_id)
    print("Client ({}) is connected to {}".format(controller.count, dbdoc_id))
    random.seed(controller.count * 1000)  # make randoms undeterministic

    def stop(phase_number):
        global arrived_count
        with stopper:
            arrived_count += 1
            with arrived:
                print("Client ({}) has finished phase_{}".format(controller.count, phase_number))
                arrived.notify()
            stopper.wait()

    ''' BEGIN PHASE_1 - Insert elements with random tags '''
    for i in range(loop_count):
        time.sleep(0.2 * random.random())  # make insertion non-deterministic
        tag_name = random.sample(tag_list, 1)[0]
        child_id = controller.insert_child(dbdoc_id, tag_name, True)[0]
        added_children_list.append(child_id)
    stop(1)

    ''' BEGIN PHASE_2 - add attributes to your OWN elements'''
    for child_id in added_children_list:
        controller.set_element_attr(child_id, 'owner', str(controller.count))
        controller.set_element_attr(child_id, 'type', str(controller.count))
    stop(2)

    ''' BEGIN PHASE_3 - Concurrently try to delete type attribute from all '''
    for i in range(loop_count * CLIENT_COUNT):
        time.sleep(0.2 * random.random())  # make deletion non-deterministic
        controller.delete_element_attr(str(i), 'type')

    stop(3)

    ''' BEGIN PHASE_4 - Randomly insert siblings with tag desc. Also set inner texts'''
    for i in range(loop_count):
        time.sleep(0.2 * random.random())
        id_ = str(random.randint(0, (loop_count * CLIENT_COUNT) - 1))
        sibling_id = controller.insert_sibling(id_, 'desc', False)[0]
        controller.set_element_text(sibling_id, str(controller.count))
    stop(4)

    ''' BEGIN PHASE 5 - Delete all elements having tag desc'''
    for i in range(loop_count * CLIENT_COUNT, loop_count * CLIENT_COUNT * 2):
        time.sleep(0.2 * random.random())
        controller.delete_element_by_id(str(i))
    stop(5)

    ''' BEGIN PHASE 6 - insert text randomly'''
    for i in range(loop_count):
        time.sleep(0.2 * random.random())
        id_ = str(random.randint(0, (loop_count * CLIENT_COUNT) - 1))
        controller.insert_text(id_, "inserted by {} in loop's {}th step\n".format(controller.count, i))
    stop(6)

    ''' BEGIN PHASE 7 - save document to database'''
    for i in range(loop_count):
        time.sleep(0.2 * random.random())
        DBDPersistency.save(controller.get_id(), safe=True)  # safe = True makes sure that save function does not
        # validate given text
    stop(7)

    ''' BEGIN PHASE 8 - randomly save document or delete an element and change name'''
    for i in range(loop_count):
        time.sleep(0.2 * random.random())
        if random.random() < 0.15:
            DBDPersistency.save(controller.get_id(), safe=True)
        else:
            controller.delete_element_by_id(added_children_list[i])
        controller.set_name("name " + str(controller.count) + " " + str(i))
    stop(8)
    ''' BEGIN PHASE 9 - randomly do database operations '''
    for i in range(loop_count):
        rand_value = random.random()
        time.sleep(0.2 * rand_value)
        if rand_value < 0.3:
            DBDPersistency.delete(controller.get_id())
            print('deleted succesfully')
        elif rand_value < 0.6:
            try:
                DBDPersistency.load(controller.get_id())
                print('Loaded succesfully!')
            except:
                print('Cannot load since the object is not saved to db.')
        else:
            DBDPersistency.save(controller.get_id(), safe=True)
            print('saved succesfully')
    is_finished = True
    stop(9)


def arrive_service():
    global stopper
    global arrived
    global CLIENT_COUNT
    global arrived_count
    global is_finished

    while is_finished is False:
        with arrived:
            while arrived_count < CLIENT_COUNT:
                arrived.wait()
        time.sleep(0.5)  # make sure each thread is waiting.
        print('All clients has finished their job. Moving on.')
        arrived_count = 0
        with stopper:
            print(dbdoc.get_name())
            print(dbdoc.dbdoc)
            while True:
                x = input('Press c to continue, d to see database content')
                if x == 'd':
                    pprint(DBDPersistency.list())
                if x == 'c':
                    stopper.notifyAll()
                    break


def empty_db():
    connection = DBConnector.get_connection()
    connection.cursor().execute('DELETE FROM xml_doc')
    connection.commit()


def run():
    empty_db()
    threads = []
    arrive_th = Thread(target=arrive_service)
    arrive_th.start()
    time.sleep(0.5)
    for i in range(CLIENT_COUNT):
        threads.append(Thread(target=client))
    for i in threads:
        i.start()
    for i in threads:
        i.join()
    arrive_th.join()
    empty_db()
