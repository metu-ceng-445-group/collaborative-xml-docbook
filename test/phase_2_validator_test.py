from dbd.DBDController import DBDController
from util.ValidationUtil import ValidationUtil


def go_to_next_step(controller):
    print(controller.dbdoc)
    print(ValidationUtil.validate(controller.dbdoc.content))
    while True:
        controller = input('press c to continue\n\n')
        if controller == 'c': return


def run():
    xml_doc_example2 = '<page id="15"></page>'
    controller = DBDController()

    controller.upload(xml_doc_example2)
    go_to_next_step(controller)

    title_id = controller.insert_child(controller.get_id(), 'title')[0]
    go_to_next_step(controller)

    controller.set_element_text(title_id, 'title inner text')
    go_to_next_step(controller)

    info_id = controller.insert_child(controller.get_id(), 'info')[0]
    go_to_next_step(controller)

    license_id = controller.insert_child(info_id, 'license')[0]
    revision_id = controller.insert_child(info_id, 'revision')[0]
    go_to_next_step(controller)

    controller.set_element_text(revision_id, ' revision text')
    go_to_next_step(controller)

    controller.set_element_text(license_id, ' license text')
    go_to_next_step(controller)

    credit_id = controller.insert_child(info_id, 'credit')[0]
    go_to_next_step(controller)

    name_id = controller.insert_child(credit_id, 'name')[0]
    go_to_next_step(controller)

    controller.set_element_text(name_id, 'name text')
    go_to_next_step(controller)

    desc_id = controller.insert_child(info_id, 'desc')[0]
    go_to_next_step(controller)

    p_id = controller.insert_child(desc_id, 'p')[0]
    go_to_next_step(controller)

    controller.set_element_text(p_id, 'p text')
    go_to_next_step(controller)

    desc_id2 = controller.insert_child(info_id, 'desc')[0]
    credit_id2 = controller.insert_sibling(credit_id, 'credit')[0]
    name_id2 = controller.insert_child(credit_id2, 'name')[0]
    controller.set_element_text(name_id2, 'name 2 text')
    go_to_next_step(controller)

    controller.delete_element_by_id(desc_id2)
    go_to_next_step(controller)

    license_id2 = controller.insert_child(info_id, 'license')[0]
    go_to_next_step(controller)
