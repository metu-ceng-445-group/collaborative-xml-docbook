import sys

try:
    option = sys.argv[1]
except IndexError:
    option = ''
if option == 'server':
    from server import server

    server.run()
elif option == 'client':
    from test import phase_2_client

    phase_2_client.run()
elif option == 'validator':
    from test import phase_2_validator_test

    phase_2_validator_test.run()
elif option == 'dummy_test':
    from test import phase_2_dummy_clients

    phase_2_dummy_clients.run()
else:
    print('Please supply a command line argument one of the following: \n')
    print('server\nclient\nvalidator\ndummy_test\n')
