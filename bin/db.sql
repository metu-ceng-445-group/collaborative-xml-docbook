CREATE DATABASE hw;

CREATE TABLE IF NOT EXISTS xml_doc (
    id VARCHAR(255),
    name VARCHAR(255),
    content TEXT,
    PRIMARY KEY (id)
);
