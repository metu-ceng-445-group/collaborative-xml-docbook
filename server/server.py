import time
from socket import *
from threading import Thread
from dbd.DBDPersistency import DBDPersistency
from dbd.DBDController import DBDController
import lxml.etree as ET

CLIENT_COUNT = 10

dbdoc = DBDController()


def notify_service(sock, controller):
    while sock:
        with controller.notification:
            try:
                controller.notification.wait()
                sock.send('Client {} altered the document you are connected to. Document updated.'.format(controller.count).encode())
            except Exception as e:  # socket is closed.
                print('Closing notification channel as well')
                break



def service(sock):
    #get document id

    while True:
        try:
            req = sock.recv(1000)
            dbdoc_id = req.decode()
        except:
            sock.close()
            print('Client connection is closed.')
            return
        try:
            controller = DBDController(dbdoc_id)
            break
        except:
            try:
                sock.send('The id you entered is invalid.'.encode())
            except:
                print('Client connection is closed.')
                sock.close()
                return

    Thread(target=notify_service, args=(sock, controller)).start()
    sock.send("DBDoc initialized, notify_service created.".encode())

    while True:
        time.sleep(0.3)
        try:
            req = sock.recv(1000).decode()
        except:
            break

        if not req or req == '':
            break
        inputs = req.split(",")

        if inputs[0] == 'set_name':
            try:
                name = inputs[1]
            except IndexError:
                sock.send("Given input is missing arguments".encode())
                continue
            name.strip(' ')
            result, message = controller.set_name(name)
            sock.send("{}\n".format(message).encode())

        elif inputs[0] == "upload":
            try:
                xmlcontent = inputs[1]
            except IndexError:
                sock.send("Given input is missing arguments".encode())
                continue
            result, message = controller.upload(xmlcontent)
            if result:
                sock.send("Given document is uploaded. \n".encode())
            else:
                sock.send("Could not upload text because of the following reason:\n{}".format(message).encode())

        elif inputs[0] == "get_id":
            sock.send("Id of the document is '{}'.\n".format(controller.get_id()).encode())

        elif inputs[0] == "get_name":
            sock.send("Name of the document is '{}'.\n".format(controller.get_name()).encode())

        elif inputs[0] == "get_element_by_id":
            try:
                element_id = inputs[1]
            except IndexError:
                sock.send("Given input is missing arguments".encode())
                continue
            element = controller.get_element_by_id(element_id)
            if element is not None:
                answer = ET.tostring(element, pretty_print=True).decode('utf-8')
                sock.send(("Requested element {} is:"+answer).format(element).encode())
            else:
                sock.send("Element with given id doesn't exist.\n".encode())

        elif inputs[0] == "get_element_by_xpath":
            try:
                xpath = inputs[1]
            except IndexError:
                sock.send("Given input is missing arguments".encode())
                continue
            element = controller.get_element_by_xpath(xpath)
            if element is not None:
                answer = ET.tostring(element, pretty_print=True).decode('utf-8')
                sock.send(("Requested element {} is:\n"+answer).format(element).encode())
            else:
                sock.send("Element with given xpath doesn't exist.\n".encode())

        elif inputs[0] == "delete_element_by_id":
            try:
                element_id = inputs[1]
            except IndexError:
                sock.send("Given input is missing arguments".encode())
                continue
            result, message = controller.delete_element_by_id(element_id)
            sock.send("{}\n".format(message).encode())

        elif inputs[0] == "delete_element_attr":
            try:
                element_id = inputs[1]
                attr = inputs[2]
            except IndexError:
                sock.send("Given input is missing arguments".encode())
                continue
            result, message = controller.delete_element_attr(element_id,attr)
            sock.send("{}\n".format(message).encode())

        elif inputs[0] == "set_element_attr":
            try:
                element_id = inputs[1]
                attr = inputs[2]
                value = inputs[3]
            except IndexError:
                sock.send("Given input is missing arguments".encode())
                continue
            result, message = controller.set_element_attr(element_id,attr,value)
            sock.send("{}\n".format(message).encode())


        elif inputs[0] == "set_element_text":
            try:
                element_id = inputs[1]
                text = inputs[2]
            except IndexError:
                sock.send("Given input is missing arguments".encode())
                continue
            result, message = controller.set_element_text(element_id,text)
            sock.send("{}\n".format(message).encode())

        elif inputs[0] == "insert_child":
            try:
                element_id = inputs[1]
                tag = inputs[2]
            except IndexError:
                sock.send("Given input is missing arguments".encode())
                continue
            try:
                append = inputs[3]
            except:
                append = True
            result, message = controller.insert_child(element_id,tag,append)
            sock.send("{}\n".format(message).encode())

        elif inputs[0] == "insert_sibling":
            try:
                element_id = inputs[1]
                tag = inputs[2]
            except IndexError:
                sock.send("Given input is missing arguments".encode())
                continue
            try:
                after = inputs[3]
            except IndexError:
                after = True
            result, message = controller.insert_child(element_id, tag, after)
            sock.send("{}\n".format(message).encode())

        elif inputs[0] == "insert_text":
            try:
                element_id = inputs[1]
                text = inputs[2]
            except IndexError:
                sock.send("Given input is missing arguments".encode())
                continue
            result, message = controller.insert_text(element_id,text)
            sock.send("{}\n".format(message).encode())

        elif inputs[0] == "save":
            try:
                safe = inputs[1]
            except:
                safe = False
            result, message_list = DBDPersistency.save(controller.get_id(),safe)
            if result:
                sock.send("Document saved".encode())
            elif "An error occured when saving document" not in message_list[0]:
                sock.send("Document could not be saved because of the following reasons:\n{}".format(message_list).encode())
            else:
                sock.send("Document could not be saved".encode())

        elif inputs[0] == "load":
            try:
                DBDPersistency.load(controller.get_id(), controller)
                sock.send("Document loaded from database".encode())
            except:
                sock.send("Document could not be loaded".encode())

        elif inputs[0] == "list":
            try:
                result = DBDPersistency.list()
                sock.send(str(result).encode())
            except:
                sock.send("Listing is not possible".encode())
        elif inputs[0] == "listmem":
            try:
                dirty = inputs[1]
            except:
                dirty = False
            try:
                result = DBDPersistency.listmem(dirty)
                sock.send(str(result).encode())
            except:
                sock.send("Listing is not possible".encode())

        elif inputs[0] == "delete":
            try:
                DBDPersistency.delete(controller.get_id())
                sock.send("Document deleted from database".encode())
            except:
                sock.send("Document could not be deleted".encode())

        elif inputs[0] == 'exit':
            sock.send(''.encode())
            break

        elif inputs[0] == 'get_document':
            sock.send("Here is your document:\n {}. \n".format(controller).encode())

        else:
            sock.send("The command you entered is invalid.".encode())

    print('Client connection is closed. {}'.format(sock.getpeername()))
    sock.close()
    with controller.notification:
        controller.notification.notifyAll()  # this is needed for client-notification thread to close.


def serve(port):
    s = socket(AF_INET, SOCK_STREAM)
    s.bind(('', port))
    s.listen(1)  # 1 is queue size for "not yet accept()'ed connections"
    try:
        for i in range(CLIENT_COUNT):
            ns, peer = s.accept()
            print(peer, "connected")
            # create a thread with new socket
            time.sleep(1)
            ns.send("Connection established".encode())
            t = Thread(target=service, args=(ns,))
            t.start()
            # now main thread ready to accept next connection
    finally:
        s.close()
def run():
    server = Thread(target=serve, args=(20446,))
    server.start()
    print("Server Started on 20466")
