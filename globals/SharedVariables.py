''' Behaves as a singleton service.
Use this class for globally defined variables.
But its main usage will be holding global dbdoc objects. '''
from pprint import pprint


class SharedVariables:

    ''' dbdoc_list stores JSON objects in form of:
    [{
        "id":{
            "changed": False,
            "content": XmlTree
            }
    }, ...]
    '''
    dbdoc_list = {}

    ''' dbdcontroller_notifier_list stores JSON objects in form of: 
    [{
        "id":notifier_functions[]    
    }, ...]
    '''
    dbdcontroller_notifier_list = {}

    @staticmethod
    def add_dbdoc(dbdoc, changed):
        dbdoc_id = dbdoc.get_id()
        SharedVariables.dbdoc_list[dbdoc_id] = {'changed': changed, 'content': dbdoc}

    @staticmethod
    def delete_dbdoc(dbdoc_id):
        try:
            SharedVariables.dbdoc_list.pop(dbdoc_id)
        finally:
            pass

    @staticmethod
    def get_dbdoc(dbdoc_id):
        try:
            return SharedVariables.dbdoc_list[dbdoc_id]['content']
        except KeyError:
            return None

    @staticmethod
    def get_all_dbdoc():
        return SharedVariables.dbdoc_list

    @staticmethod
    def add_dbdcontroller_notifier(dbdoc_id, notifier, callback):
        try:
            notifier_list = SharedVariables.dbdcontroller_notifier_list[dbdoc_id]
            if notifier_list is None:
                notifier_list = []
            notifier_list.append((notifier, callback))
            SharedVariables.dbdcontroller_notifier_list[dbdoc_id] = notifier_list
        except KeyError:
            SharedVariables.dbdcontroller_notifier_list[dbdoc_id] = [(notifier, callback)]


    ''' This method should only be called by DBDController objects but nothing more.'''
    @staticmethod
    def notify_controllers_on_change(dbdoc_id, dbdoc, loaded=False, owner_notification = None):
        if SharedVariables.dbdoc_list[dbdoc_id] is not None:
            SharedVariables.dbdoc_list[dbdoc_id]['changed'] = loaded is False
            SharedVariables.dbdoc_list[dbdoc_id]['content'] = dbdoc
            for key in SharedVariables.dbdcontroller_notifier_list:
                if key != dbdoc_id:
                    continue
                notifier_list = SharedVariables.dbdcontroller_notifier_list[key]
                for notifier,callback in notifier_list:
                    with notifier:
                        callback()
                        if owner_notification is not None and owner_notification == notifier:
                            continue
                        notifier.notify()

                        # notify all the DBDControllers which are attached to the xml with this id.

    ''' call this method when db_doc is saved to db '''
    @staticmethod
    def on_db_save(dbdoc_id):
        try:
            SharedVariables.dbdoc_list[dbdoc_id]['changed'] = False
        except:
            pass
    @staticmethod
    def to_string():
        return str(SharedVariables.dbdoc_list) + '\n' + str(SharedVariables.dbdcontroller_notifier_list)
